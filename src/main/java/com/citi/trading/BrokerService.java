
package com.citi.trading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/accounts")
public class BrokerService {
	private Map<Integer,Investor> investors = new HashMap<>();
	private int investorID = 0;
	@Autowired 
	private ApplicationContext applicationContext;
	@PostConstruct
	public void init() {
		Map<String,Integer> starter1 = new HashMap<>();
		Map<String,Integer> starter2 = new HashMap<>();
		starter2.put("MSFT", 10000);
		Map<String,Integer> starter3 = new HashMap<>();
		starter3.put("MRK", 20000);
		openAccount(100000, starter1);
		openAccount(200000, starter2);
		openAccount(300000, starter3);

		System.out.println("Starting the server...");
	}

	@GetMapping()   
	public List<Investor> getAll(){
		//TODO - return json 
		return new ArrayList<Investor>(investors.values());
	}
	
	@GetMapping("/{ID}")
	public ResponseEntity<String> getByID(@PathVariable("ID") int ID) {
		//return investors.get(ID);
		if (!investors.containsKey (ID)) {
			return new ResponseEntity<String> 
            ("KEY NOT FOUND: " + String.valueOf(ID), HttpStatus.NOT_FOUND);
		}
		
		String str = String.valueOf(investors.get(ID)); 
		System.out.println(str);
		return new ResponseEntity<String> (str, HttpStatus.OK);
		
	}
	
	//http://localhost:8081/accounts/openAccount?cash=100
	@RequestMapping(value="/openAccount",method=RequestMethod.POST)
	public Investor openAccount(@RequestParam("cash") double cash, Map<String,Integer> portfolio) {
		Investor new_account = applicationContext.getBean(Investor.class);
		int new_ID = investorID+1;
		new_account.setID(new_ID);
		new_account.setCash(cash);
		new_account.setPortfolio(portfolio);
		investors.put(new_ID, new_account);
		investorID ++;
		return new_account;
		
	}
	
	//http://localhost:8081/accounts/deleteAccount?ID=1
	@RequestMapping(value="/deleteAccount",method=RequestMethod.DELETE)
	public ResponseEntity<String> closeAccount(@RequestParam("ID") int ID) {
		
		if (!investors.containsKey (ID)) {
			return new ResponseEntity<String> 
            ("KEY NOT FOUND: " + String.valueOf(ID), HttpStatus.NOT_FOUND);
		}
		
		investors.remove(ID);
		System.out.println(investors);
		String str = "Investor " + String.valueOf(ID) + " has been removed"; 
		return new ResponseEntity<String> (str, HttpStatus.OK);
	}
	
}