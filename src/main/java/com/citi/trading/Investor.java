package com.citi.trading;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Encapsulates a stock investor with a portfolio of stocks, some cash,
 * and the ability to place trade orders.
 * 
 * @author Will Provost
 */
@Scope("prototype")
@Component
public class Investor {
	
	private int ID;

	private Map<String,Integer> portfolio;
	private double cash;
	@Autowired
	private OrderPlacer market;
	
	

	/**
	 * Handler for trade confirmations.
	 */
	private class NotificationHandler implements Consumer<Trade> {
		public void accept(Trade trade) {
			System.out.println("entered accept!!!!!!!!!!!!!!!!!");
			if (trade.getSize() != 0 && trade.getResult() != Trade.Result.REJECTED) {
				synchronized(Investor.this) {
					String stock = trade.getStock();
					if (trade.isBuy()) {
						if (!portfolio.containsKey(stock)) {
							portfolio.put(stock, 0);
						}
						portfolio.put(stock, portfolio.get(stock) + trade.getSize());
						cash -= trade.getPrice() * trade.getSize();
						System.out.println("entered buy#########################");
						
					} else {
						int shares = portfolio.get(stock) - trade.getSize();
						if (shares != 0) {
							portfolio.put(stock, shares);
						} else {
							portfolio.remove(stock);
						}
						cash += trade.getPrice() * trade.getSize();
						System.out.println("entered sell???????????????????????????");
					}
				}
			}
		}
	}
	private NotificationHandler handler = new NotificationHandler();
	
	public int getID() {
		return ID;
	}

	public void setID(int ID) {
		this.ID = ID;
	}
	
	public void setCash(double cash) {
		if (cash < 0) {
			throw new IllegalArgumentException("Can't have negative cash.");
		}
		this.cash = cash;
	}
	
	public void setPortfolio(Map<String,Integer> portfolio) {
//		if (portfolio != null && portfolio.size() == 0) {
//			this.portfolio = portfolio;
//		}
//		else {
			for (int shares : portfolio.values()) {
				
				if (shares <= 0) {
					throw new IllegalArgumentException("All share counts must be positive.");
				}
			}
			this.portfolio = portfolio;
//		}
		
		
	}
	
	
//	public void setPortfolio() {
//		this.portfolio = new HashMap<String,Integer>();
//	}
	/** INIT CODE ##########################
	
	 //Create an investor with some cash but no holdings.
	 
	public Investor(double cash, OrderPlacer market) {
		this(new HashMap<>(), cash, market);
	}
	
	//
	 // Create an investor with holdings as a map of tickers and share counts,
	 // and some cash on hand.
	 
	public Investor(Map<String,Integer> portfolio, double cash, OrderPlacer market) {
		if (cash < 0) {
			throw new IllegalArgumentException("Can't have negative cash.");
		}
		for (int shares : portfolio.values()) {
			if (shares <= 0) {
				throw new IllegalArgumentException("All share counts must be positive.");
			}
		}
		
		this.portfolio = portfolio;
		this.cash = cash;
		this.market = market;
	}
	#################################################**/
	
	/**
	 * Accessor for the portfolio.
	 */
	public Map<String,Integer> getPortfolio() {
		return portfolio;
	}
	
	/**
	 * Accessor for current cash. 
	 */
	public double getCash() {
		return cash;
	}
	
	/**
	 * Places an order to buy the given stock.
	 */
	public synchronized void buy(String stock, int size, double price) {
		if (size <= 0) {
			throw new IllegalArgumentException("Must buy a positive number of shares.");
		}
		if (size * price > cash) {
			throw new IllegalStateException("Not enough cash to make this purchase.");
		}
		
		Trade trade = new Trade(stock, true, size, price);
		System.out.println("investor buys............");
		market.placeOrder(trade, handler);
	}
	
	/**
	 * Places an order to sell the given stock.
	 */
	public synchronized void sell(String stock, int size, double price) {
		if (size <= 0) {
			System.out.println("1 exept in sell............");
			throw new IllegalArgumentException("Must sell a positive number of shares.");
		}
		if (!portfolio.containsKey(stock) || size > portfolio.get(stock)) {
			throw new IllegalStateException("Not enough shares to make this sale.");
		}
		
		Trade trade = new Trade(stock, false, size, price);
		System.out.println("investor sells............");
		market.placeOrder(trade, handler);
	}

	@Override
	public String toString() {
		return String.format("Investor: [cash=%1.4f, portfolio=%s]", 
				cash, portfolio.toString());
	}
}
