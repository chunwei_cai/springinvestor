package com.citi.trading.pricing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.function.Function;

/**
 * Client component for the pricing service. Holds a publish/subscribe registry
 * so that many subscribers can be notified of pricing data on a given stock,
 * based on a single request to the remote service. When configured as a Spring bean,
 * this component will make HTTP requests on a 15-second timer using 
 * Spring scheduling (if enabled).
 * 
 * Requires a property that provides the URL of the remote service. 
 * 
 * @author Will Provost
 */



@Component
public class Pricing {

	public static final int MAX_PERIODS_WE_CAN_FETCH = 120;
	public static final int SECONDS_PER_PERIOD = 15;
	
	private static final Logger LOG = Logger.getLogger(Pricing.class.getName());
	
	public static PricePoint parsePricePoint(String CSV) {
		SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String[] fields = CSV.split(",");
		if (fields.length < 6) {
			throw new IllegalArgumentException
				("There must be at least 6 comma-separated fields: '" + CSV + "'");
		}
		
		try {
			Timestamp timestamp = new Timestamp(parser.parse(fields[0]).getTime());
			double open = Double.parseDouble(fields[1]);
			double high = Double.parseDouble(fields[2]);
			double low = Double.parseDouble(fields[3]);
			double close = Double.parseDouble(fields[4]);
			int volume = Integer.parseInt(fields[5]);
			
			return new PricePoint(timestamp, open, high, low, close, volume);
		} catch (Exception ex) {
			throw new RuntimeException("Couldn't parse timestamp.", ex);
		}
	}
	
	/**
	 * Factory interface that allows the component to "know" and to supply
	 * the request URLs it needs, while delegating the responsibility of
	 * creating a component that can make the HTTP connection.
	 */
	@FunctionalInterface
	public static interface URLFactory {
		public URL fromURLString(String requestURL) throws MalformedURLException;
	}
	
	//@Autowired
	private Function<String,InputStream> connector;
	private Map<String,PriceData> priceData  = new HashMap<>();
	private Map<String,List<Consumer<PriceData>>> subscribers = new HashMap<>();
	

	interface Connector extends Function<String,InputStream> {
		
	}
	
//	@Autowired
	//public Connector connector;
	
	
	@Value("${com.citi.trading.pricing.Pricing.serviceURL}")
	String requestURL;
	
	/**
	 * By default, we supply a factory that creates an ordinary
	 * <strong>java.net.URL</strong> object and opens it, returning
	 * the HTTP input stream.
	 */
	
	
	//public Pricing() {
	
	@PostConstruct
	public void init () {
		connector = (requestURL -> {
			try {
				return new URL(requestURL).openStream();
			} catch (Exception ex) {
				throw new RuntimeException("couldn't create URL connection.", ex);
			}
		});
	}
	
	/**
	 * Use this constructor to supply your own connection factory.
	 */
//	public Pricing(Function<String,InputStream> connector) {
//		this.connector = connector;
//	}
	
	/**
	 * If there are no other subscribers for this stock, creates a new 
	 * {@link PriceData} of the requested capacity, registers it for the given
	 * symbol, and registers the subscriber. If there are other subscribers, 
	 * then just adds this subscriber to the list for this symbol, 
	 * and expands the data structure if necessary.
	 */
	public synchronized void subscribe(String symbol, int capacity, Consumer<PriceData> subscriber) {
		
		if (!subscribers.containsKey(symbol)) {
			subscribers.put(symbol, new ArrayList<>());
		}
		subscribers.get(symbol).add(subscriber);
		
		if (!priceData.containsKey(symbol)) {
			priceData.put(symbol, new PriceData(symbol, capacity));
		} else if (priceData.get(symbol).getCapacity() < capacity) {
			priceData.get(symbol).expandTo(capacity);
		}
		
		LOG.info("New subscriber for " + symbol);
	}
	
	/**
	 * Removes the subscriber, and if this leaves no other subscribers for
	 * this stock, removes the entry for this stock and lets go of the
	 * associated {@link PriceData}.
	 */
	public synchronized void unsubscribe(String symbol, Consumer<PriceData> subscriber) {
		
		if (subscribers.containsKey(symbol)) {
			if (subscribers.get(symbol).contains(subscriber)) {
				subscribers.get(symbol).remove(subscriber);
				if (subscribers.get(symbol).isEmpty()) {
					subscribers.remove(symbol);
					priceData.remove(symbol);
				}
			} else {
				throw new IllegalStateException("No such subscriber for symbol: " + symbol);
			}
		} else {
			throw new IllegalStateException("No subscribers for symbol: " + symbol);
		}
		
		LOG.info("Removed subscriber for " + symbol);
	}

	/**
	 * For each stock for which we have subscribers, requests price data.
	 * If this is our first request for this stock (or first after the data 
	 * structure was expanded) then ask for enough periods to fill the structure,
	 * subject to a cap on number that can be requested imposed by the remote
	 * service. If we already have some data, then ask for the most recent 
	 * price point, and add that to our data.
	 * 
	 * Then save new data and advise each subscriber for that symbol with the new data.
	 */
	public synchronized void getPriceData() {
		try {
			LOG.info("Requesting pricing for : " +
					subscribers.keySet().stream().collect(Collectors.joining(" ")));
			for (String symbol : subscribers.keySet()) {
				PriceData prices = priceData.get(symbol);
				int periods = prices.getSize() == 0
						? Math.min(prices.getCapacity(), MAX_PERIODS_WE_CAN_FETCH)
						: 1;
				
				String requestURL = "http://will1.conygre.com:8081/prices/" + symbol + "?periods=" + periods;
				//http://will1.conygre.com:8081/prices/msft?periods=3
				BufferedReader in = new BufferedReader(new InputStreamReader
						(connector.apply(requestURL)));
				String line = null;
				List<PricePoint> pricePoints = new ArrayList<>();
				while ((line = in.readLine()) != null) {
					if (!line.startsWith("timestamp")) {
						PricePoint price = Pricing.parsePricePoint(line);
						price.setStock(symbol);
						pricePoints.add(price);
					}
				}
				
				if (prices.addData(pricePoints)) {
					for (Consumer<PriceData> subscriber : subscribers.get(symbol)) {
						subscriber.accept(prices);
					}
				}
			}
		} catch (IOException ex) {
			throw new RuntimeException
					("Couldn't retrieve price .", ex);
		}
	}

	public void setConnector(Connector myConnector) {
		// TODO Auto-generated method stub
		this.connector = myConnector;
	}
}
