package com.citi.trading;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.trading.InvestorTest.MockMarket;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes=BrokerServiceTest.Config.class)
@DirtiesContext(classMode=ClassMode.BEFORE_EACH_TEST_METHOD)
public class BrokerServiceTest {

	@Autowired
	private BrokerService bs;
	@Autowired
	private MockMarket mockMarket;
	@Autowired
	private Investor investor;

	@Configuration
	@ComponentScan
	public static class Config {
		@Bean
		@Scope("prototype")
		public BrokerService brokerService() {
			// Map<String, Integer> starter = new HashMap<String, Integer>();
			BrokerService bs = new BrokerService();
			return bs;
		}
		
		@Bean
		@Scope("prototype")
		public Investor investor() {
			return new Investor();
		}
		
		@Bean
		@Scope("prototype")
		public MockMarket mockMarket() {
			return new MockMarket();
		}
	}
	@Test
	public void testGetAll() {
		assertThat(bs.getAll(),hasSize(3));
	}
	@Test
	public void testOpenMultipleAccounts() {
		Map<String,Integer> starter = new HashMap<>();
		starter.put("MSFT", 10000);
		Investor new_account1 = bs.openAccount(10000, starter);
		
		Map<String,Integer> starter2 = new HashMap<>();
		starter2.put("MSFT", 10000);
		Investor new_account2 = bs.openAccount(10000, starter2);
		//assertThat(investor.getPortfolio(), not(hasKey(equalTo("MSFT"))));
		
		assertThat(bs.getAll(),hasSize(5));
		assertThat(bs.getAll(), hasItem(equalTo(new_account1)));
		assertThat(bs.getAll(), hasItem(equalTo(new_account2)));
	}
	@Test
	public void testCloseAccount() {
		
		bs.closeAccount(3);
		assertThat(bs.getAll(), hasSize(2));
	}
	
//	@Test
//	public void testGetByID() {
//
//		Investor investor = bs.getByID(2);
//		assertThat(investor.getPortfolio(), hasEntry(equalTo("MSFT"),equalTo(10000)));
//	}

	
}
